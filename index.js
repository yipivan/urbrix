const puppeteer = require('puppeteer')
const { worker_number, targetUrl, targetSelector } = require('./config')

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

(async () => {
  let browser = await puppeteer
    .launch({
      headless: false,
    })

    [...Array(worker_number).keys()].forEach(async () => {
      while (true) {
        let selector
        do {
          let page
          try {
            page = await browser.newPage()
            await page.goto(targetUrl)
            selector = await page.waitForSelector(targetSelector)
            await sleep(3000)
            await page.close()
          } catch (err) {
            await sleep(3000)
            page.close()
          }
        } while (selector != null)
      }
    })
})()

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}
